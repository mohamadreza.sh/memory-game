package com.example.memorygame;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class FirstActivity extends AppCompatActivity {

    private TextView txtShow;
    private Button btn0;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        txtShow = findViewById(R.id.txtShow);
        btn0 = findViewById(R.id.btn0);
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btn5 = findViewById(R.id.btn5);
        btn6 = findViewById(R.id.btn6);
        btn7 = findViewById(R.id.btn7);
        btn8 = findViewById(R.id.btn8);

        final Intent intent = new Intent(FirstActivity.this, MainActivity.class);
        intent.putExtra("status", "level");


        View.OnClickListener onClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent1 = new Intent(FirstActivity.this, MainActivity.class);

                int level = 0;

                switch (v.getId()) {
                    case R.id.btn0:
                        level = 0;
                        break;
                    case R.id.btn1:
                        level = 1;
                        break;
                    case R.id.btn2:
                        level = 2;
                        break;
                    case R.id.btn3:
                        level = 3;
                        break;
                    case R.id.btn4:
                        level = 4;
                        break;
                    case R.id.btn5:
                        level = 5;
                        break;
                    case R.id.btn6:
                        break;
                    case R.id.btn7:
                        break;
                    case R.id.btn8:
                        break;

                }
                intent.putExtra("level", level);
                startActivity(intent1);

            }
        };

        btn0.setOnClickListener(onClickListener);
        btn1.setOnClickListener(onClickListener);
        btn2.setOnClickListener(onClickListener);
        btn3.setOnClickListener(onClickListener);
        btn4.setOnClickListener(onClickListener);
        btn5.setOnClickListener(onClickListener);
        btn6.setOnClickListener(onClickListener);
        btn7.setOnClickListener(onClickListener);
        btn8.setOnClickListener(onClickListener);


    }

}
