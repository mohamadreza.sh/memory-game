package com.example.memorygame;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private Button btn0;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;
    private Button btn10;
    private Button btn11;
    private Button btn12;
    private Button btn13;
    private Button btn14;
    private LinearLayout line1;
    private LinearLayout line2;
    private LinearLayout line3;
    private int countTrueButton;
    private int countLevel;
    private int countScore;
    private int levelCountButton = 5;


    private int[] a;

    private int level;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn0 = findViewById(R.id.btn0);
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btn5 = findViewById(R.id.btn5);
        btn6 = findViewById(R.id.btn6);
        btn7 = findViewById(R.id.btn7);
        btn8 = findViewById(R.id.btn8);
        btn9 = findViewById(R.id.btn9);
        btn10 = findViewById(R.id.btn10);
        btn11 = findViewById(R.id.btn11);
        btn12 = findViewById(R.id.btn12);
        btn13 = findViewById(R.id.btn13);
        btn14 = findViewById(R.id.btn14);
        line1 = findViewById(R.id.line1);
        line2 = findViewById(R.id.line2);
        line3 = findViewById(R.id.line3);

        level = getIntent().getIntExtra("level", 0);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonPosition((Button) v);
            }
        };

        btn0.setOnClickListener(onClickListener);
        btn1.setOnClickListener(onClickListener);
        btn2.setOnClickListener(onClickListener);
        btn3.setOnClickListener(onClickListener);
        btn4.setOnClickListener(onClickListener);
        btn5.setOnClickListener(onClickListener);
        btn6.setOnClickListener(onClickListener);
        btn7.setOnClickListener(onClickListener);
        btn8.setOnClickListener(onClickListener);
        btn9.setOnClickListener(onClickListener);
        btn10.setOnClickListener(onClickListener);
        btn11.setOnClickListener(onClickListener);
        btn12.setOnClickListener(onClickListener);
        btn13.setOnClickListener(onClickListener);
        btn14.setOnClickListener(onClickListener);

        nextLevel(level);
    }


    public void randomElements(int size) {
        a = new int[size];

        for (int i = 0; i < size; i++) {
            a[i] = (int) (Math.random() * levelCountButton * 3);

            for (int j = 0; j < i; j++) {
                if (a[i] == a[j]) {
                    i--;
                    break;
                }
            }
        }


        for (int i = 0; i < a.length; i++) {
            final Button button = findButtonByPosition(a[i]);
            button.setBackgroundResource(R.drawable.circle_blue_dark);

            button.postDelayed(new Runnable() {
                @Override
                public void run() {
                    button.setBackgroundResource(R.drawable.circle_blue_light);
                }
            }, 1200);
        }
    }

    private Button findButtonByPosition(int position) {
        Button button;

        LinearLayout line;
        int childPosition;

        if (position < levelCountButton) {
            line = line1;
            childPosition = position;
        } else if (position < (levelCountButton * 2)) {
            line = line2;
            childPosition = position - levelCountButton;
        } else {
            childPosition = position - (levelCountButton * 2);
            line = line3;
        }

        Log.d("log", "position " + position + "   childPosition:  " + childPosition);

        button = (Button) line.getChildAt(childPosition);

        return button;
    }

    private void buttonPosition(Button button) {
        Handler handler = new Handler();
        if (isLevelFinished()) {
            return;
        }

        int currentPosition = 0;

        switch (button.getId()) {
            case R.id.btn0:
                currentPosition = 0;
                break;
            case R.id.btn1:
                currentPosition = 1;
                break;
            case R.id.btn2:
                currentPosition = 2;
                break;
            case R.id.btn3:
                currentPosition = 3;
                break;
            case R.id.btn4:
                currentPosition = 4;
                break;
            case R.id.btn5:
                currentPosition = 5;
                break;
            case R.id.btn6:
                currentPosition = 6;
                break;
            case R.id.btn7:
                currentPosition = 7;
                break;
            case R.id.btn8:
                currentPosition = 8;
                break;
            case R.id.btn9:
                currentPosition = 9;
                break;
            case R.id.btn10:
                currentPosition = 10;
                break;
            case R.id.btn11:
                currentPosition = 11;
                break;
            case R.id.btn12:
                currentPosition = 12;
                break;
            case R.id.btn13:
                currentPosition = 13;
                break;
            case R.id.btn14:
                currentPosition = 14;
                break;
        }

        if (currentPosition > 4 && currentPosition < 10) {
            currentPosition = currentPosition - (5 - levelCountButton);
        } else if (currentPosition > 9) {
            currentPosition = currentPosition - ((5 - levelCountButton) * 2);
        }

        boolean hasPosition = false;

        for (int i = 0; i < a.length; i++) {
            if (currentPosition == a[i]) {
                hasPosition = true;
                break;
            }

        }

        if (hasPosition) {
            button.setBackgroundResource(R.drawable.circle_blue_dark);
            countTrueButton++;
            countScore += 10;

            if (countTrueButton == a.length) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        next();
                    }
                }, 1000);
            }
        } else {
            Toast.makeText(this, "Loose", Toast.LENGTH_SHORT).show();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    next();
                }
            }, 1000);
            countScore -= 5;
        }
    }

    private void next() {
        countLevel++;

        resetViews();

        if (isLevelFinished()) {
            Toast.makeText(this, "Finish level 1", Toast.LENGTH_SHORT).show();
            dialog();
            return;
        }

        randomElements(levelCountButton);
    }

    private boolean isLevelFinished() {
        return countLevel > 3;
    }

    private void resetViews() {
        countTrueButton = 0;
        btn0.setBackgroundResource(R.drawable.circle_blue_light);
        btn1.setBackgroundResource(R.drawable.circle_blue_light);
        btn2.setBackgroundResource(R.drawable.circle_blue_light);
        btn3.setBackgroundResource(R.drawable.circle_blue_light);
        btn4.setBackgroundResource(R.drawable.circle_blue_light);
        btn5.setBackgroundResource(R.drawable.circle_blue_light);
        btn6.setBackgroundResource(R.drawable.circle_blue_light);
        btn7.setBackgroundResource(R.drawable.circle_blue_light);
        btn8.setBackgroundResource(R.drawable.circle_blue_light);
        btn9.setBackgroundResource(R.drawable.circle_blue_light);
        btn10.setBackgroundResource(R.drawable.circle_blue_light);
        btn11.setBackgroundResource(R.drawable.circle_blue_light);
        btn12.setBackgroundResource(R.drawable.circle_blue_light);
        btn13.setBackgroundResource(R.drawable.circle_blue_light);
        btn14.setBackgroundResource(R.drawable.circle_blue_light);
    }


    private void dialog() {
        TextView txtScore;
        ImageView imgMenu;
        ImageView imgPlay;
        ImageView imgRestart;
        ImageView imgCover;
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        View view = getLayoutInflater().inflate(R.layout.finish_dialog, null);
        builder.setView(view);
        final AlertDialog dialog = builder.create();

        txtScore = view.findViewById(R.id.txtScore);
        imgMenu = view.findViewById(R.id.imgMenu);
        imgPlay = view.findViewById(R.id.imgPlay);
        imgRestart = view.findViewById(R.id.imgRestart);
        imgCover = view.findViewById(R.id.imgCover);
        imgCover.setImageResource(R.drawable.header);

        txtScore.setText("" + countScore);

        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FirstActivity.class);
                startActivity(intent);
                finish();

            }
        });

        imgPlay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                level++;
                dialog.dismiss();
                nextLevel(level);
            }

        });
        imgRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                restart();
            }
        });


        dialog.show();


    }

    private void nextLevel(int level) {
        btn3.setVisibility(View.VISIBLE);
        btn4.setVisibility(View.VISIBLE);
        btn8.setVisibility(View.VISIBLE);
        btn9.setVisibility(View.VISIBLE);
        btn13.setVisibility(View.VISIBLE);
        btn14.setVisibility(View.VISIBLE);

        if (level == 0) {
            levelCountButton = 3;
            btn3.setVisibility(View.INVISIBLE);
            btn4.setVisibility(View.INVISIBLE);
            btn8.setVisibility(View.INVISIBLE);
            btn9.setVisibility(View.INVISIBLE);
            btn13.setVisibility(View.INVISIBLE);
            btn14.setVisibility(View.INVISIBLE);
        } else if (level == 1) {
            levelCountButton = 4;
            btn4.setVisibility(View.INVISIBLE);
            btn9.setVisibility(View.INVISIBLE);
            btn14.setVisibility(View.INVISIBLE);
        } else if (level == 2) {
            levelCountButton = 3;
            btn4.setVisibility(View.INVISIBLE);
            btn9.setVisibility(View.INVISIBLE);
            btn14.setVisibility(View.INVISIBLE);
        } else if (level == 3) {
            levelCountButton = 4;
            btn4.setVisibility(View.INVISIBLE);
            btn9.setVisibility(View.INVISIBLE);
            btn14.setVisibility(View.INVISIBLE);
        } else {
            levelCountButton = 5;
        }

        restart();
    }

    private void restart() {
        resetViews();
        countScore = 0;
        countLevel = 0;
        next();
    }

}
